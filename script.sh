#!/bin/bash

# Set terminal colors
bold=$(tput bold)
red=$(tput setaf 1)
redbold=${bold}${red}
green=$(tput setaf 2)
greenbold=${bold}${green}
normal=$(tput sgr0)

# VARS
git="git add . "
gitc="git commit -m '${commit}'"
gitp="git push"
text1="You chose for = "
opt1="data.nanohost.nl"
opt1d="ssh -l root ${opt1}"
opt1l="tee -a log/${opt1}/$(date +'%m-%d-%y-%R')-$(whoami).log"
opt2="master.nanohost.nl"
opt3="srv01.nanohost.nl"
opt4="dev.nanohost.nl"


echo ""
echo ""
PS3="

Please choose a server:
=> "
options=("$opt1" "$opt2" "$opt3" "$opt4" "log" "exit")
select opt in "${options[@]}"
do
  case $opt in
    "${opt1}")
        echo "${text1} ${opt1}";
        echo "";
	mkdir -p log/${opt1};
#        ssh -l root ${opt1} | tee -a log/${opt1}/$(date +"%m-%d-%y-%R")-$(whoami).log
	${opt1d} | ${opt1l} && ${git};
	${gitc};
	${gitp}
        ;;
    "${opt2}")
        echo "${text1} ${opt2}";
        echo "";
        ssh -l root ${opt2}
        ;;
    "${opt3}")
        echo "${text1} ${opt3}";
        echo "";
        ssh -l root ${opt3}
        ;;
    "${opt4}")
        echo " ${text1} ${opt4}";
        echo "";
        ssh -l root ${opt4}
        ;;
    "log")
        echo "What have you done?";
        read -e -p "=>" commit;
# 	git add log . ;
#	git commit -m "${commit}"
#	git status && git push	
        ;;

    "exit")
        echo " ${greenbold}

  ===========
    bye bye
  ===========
        ";
        break
      ;;
    *) echo -e " ${redbold}

 ============================================
   invalid input, you are dumb.... Go home!
 ============================================
    "\
    ;;
  esac
done
