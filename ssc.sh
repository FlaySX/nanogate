#!/bin/bash

# Set terminal colors
bold=$(tput bold)
red=$(tput setaf 1)
redbold=${bold}${red}
green=$(tput setaf 2)
greenbold=${bold}${green}
normal=$(tput sgr0)

# Hosts
# 172.16.12.5 - ssc-as-01
# 172.16.12.6 - ssc-as-02
# 172.16.12.7 - ssc-as-03
# 172.16.12.8 - ssc-as-04
# 172.16.12.9 - ssc-as-05
# 172.16.12.10 - ssc-as-06
# 172.16.12.11 - ssc-as-07
# 172.16.12.12 - ssc-as-08
# 172.16.12.13 - ssc-as-09
# 172.16.12.14 - ssc-as-10
# 172.16.12.254 - ssc-cs-01
# 172.16.254.1 - firewall

# Text
text1="You chose for = "

# logformat
logformat="$(date +'%d-%m-%y-%R')-$(whoami).log"

# Hosts-Vars
#opt1ip="172.16.12.5"
#opt1name="ssc-as-01"
#opt1l="tee -a log/${opt1name}/${logformat}"

opt1ip="data.nanohost.nl"
opt1name="data.nanohost.nl"
opt1l="tee -a log/${opt1name}/${logformat}"

opt2ip="172.16.12.6"
opt2name="ssc-as-02"
opt2l="tee -a log/${opt2name}/${logformat}"

opt3ip="172.16.12.7"
opt3name="ssc-as-03"
opt3l="tee -a log/${opt3name}/${logformat}"

opt4ip="172.16.12.8"
opt4name="ssc-as-04"
opt4l="tee -a log/${opt4name}/${logformat}"

opt5ip="172.16.12.9"
opt5name="ssc-as-05"
opt5l="tee -a log/${opt5name}/${logformat}"

opt6ip="172.16.12.10"
opt6name="ssc-as-06"
opt6l="tee -a log/${opt6name}/${logformat}"

opt7ip="172.16.12.11"
opt7name="ssc-as-07"
opt7l="tee -a log/${opt7name}/${logformat}"

opt8ip="172.16.12.12"
opt8name="ssc-as-08"
opt8l="tee -a log/${opt8name}/${logformat}"

opt9ip="172.16.12.13"
opt9name="ssc-as-09"
opt9l="tee -a log/${opt9name}/${logformat}"

opt10ip="172.16.12.14"
opt10name="ssc-as-10"
opt10l="tee -a log/${opt10name}/${logformat}"

opt11ip="172.16.12.254"
opt11name="ssc-cs-01"
opt11l="tee -a log/${opt11name}/${logformat}"

opt12ip="172.16.254.1"
opt12name="Firewall-01"
opt12l="tee -a log/${opt12name}/${logformat}"

echo ""
echo ""
PS3="

Please choose a server:
=> "
options=("$opt1name" "$opt2name" "$opt3name" "$opt4name" "$opt5name" "$opt6name" "$opt7name" "$opt8name" "$opt9name" "$opt10name" "$opt11name" "$opt12name" "Log" "Exit" )
select opt in "${options[@]}"
do
  case $opt in
    "${opt1name}")
        echo "${text} ${opt1name}";
        echo "";
	read -e -p "Wat ga je doen? => " commit;
	mkdir -p log/${opt1name};
	ssh -l root ${opt1ip} | ${opt1l};
	git add .;
	git commit -m "${commit}";
	git push
	;;
    "${opt2name}")
        echo "${text} ${opt2name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt2name};
        ssh -l netwerk ${opt2ip} | ${opt2l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;
    "${opt3name}")
        echo "${text} ${opt3name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt3name};
        ssh -l netwerk ${opt3ip} | ${opt3l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;
    "${opt4name}")
        echo "${text} ${opt4name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt4name};
        ssh -l netwerk ${opt4ip} | ${opt4l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;
    "${opt5name}")
        echo "${text} ${opt6name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt1name};
        ssh -l netwerk ${opt6ip} | ${opt6l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;

    "${opt6name}")
        echo "${text} ${opt6name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt6name};
        ssh -l netwerk ${opt6ip} | ${opt6l};
        git add . ;
        git commit -m "${commit}";
        git push
        ;;

    "${opt7name}")
        echo "${text} ${opt7name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt7name};
        ssh -l netwerk ${opt7ip} | ${opt7l};
        git add . ;
        git commit -m "${commit}";
        git push
        ;;
    "${opt8name}")
        echo "${text} ${opt8name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt8name};
        ssh -l netwerk ${opt8ip} | ${opt8l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;
    "${opt9name}")
        echo "${text} ${opt9name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt9name};
        ssh -l netwerk ${opt9ip} | ${opt9l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;
    "${opt10name}")
        echo "${text} ${opt10name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt10name};
        ssh -l netwerk ${opt10ip} | ${opt10l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;
    "${opt11name}")
        echo "${text} ${opt11name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt11name};
        ssh -l netwerk ${opt11ip} | ${opt11l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;
    "${opt12name}")
        echo "${text} ${opt12name}";
        echo "";
        read -e -p "Wat ga je doen? => " commit;
        mkdir -p log/${opt12name};
        ssh -l root  ${opt12ip} | ${opt12l};
        git add .;
        git commit -m "${commit}";
        git push
        ;;	
    "Log")
        echo "Wat heb je gedaan?";
        read -e -p "=>" commit;
	git add log . ;
	git commit -m "${commit}";
	git push
        ;;

    "Exit")
        echo "

  ===========
    bye bye
  ===========
        ";
        break
      ;;
    *) echo -e "

 ============================================
   invalid input, you are dumb.... Go home!
 ============================================
    "
    ;;
  esac
done
